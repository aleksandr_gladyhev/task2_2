﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBySpaceScript : MonoBehaviour {	
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Space)){
			Vector3 endPos = new Vector3 (0.0f,2.5f,0.0f);			
			transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.Escape)){
			transform.localScale -= new Vector3 (0.2f,0.2f,0.2f)*Time.deltaTime;
			var scale = transform.localScale;
			Debug.Log(scale);
		}
	}
}
